﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Travel.Common.CustomReflection
{
    public static class ModelBuilderExtention
    {
        public static void AddDbSet<BaseType>(this ModelBuilder builder, params Assembly[] assemblies)
        {
            IEnumerable<Type> types = assemblies.SelectMany(x => x.GetExportedTypes())
                .Where(x => x.IsPublic && !x.IsAbstract && x.IsClass && typeof(BaseType).IsAssignableFrom(x));
            foreach (Type type in types)
                builder.Entity(type);
        }
    }
}
