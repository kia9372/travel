﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Travel.Common.CustomReflection
{
    public static class ReflectionExtentions
    {
        public static IEnumerable<PropertyInfo> CustomGetProperties(this object obj)
        {
            return obj.GetType().GetProperties();
        }
    }
}
