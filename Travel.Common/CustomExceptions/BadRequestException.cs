﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Common.CustomExtentions.Enums;

namespace Travel.Common.CustomExceptions
{
    public class BadRequestException : AppException
    {
        public BadRequestException(string Message)
            : base(Message, StatusCode.BadRequest)
        {
        }
    }
}
