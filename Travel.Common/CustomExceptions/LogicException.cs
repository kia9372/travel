﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Common.CustomExtentions.Enums;

namespace Travel.Common.CustomExceptions
{
    public class LogicException : AppException
    {
        public LogicException(string Message, StatusCode StatusCode)
            : base(Message, StatusCode.LogicError)
        {
        }
    }
}
