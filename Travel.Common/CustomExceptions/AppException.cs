﻿using Travel.Common.CustomExtentions.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.Common.CustomExceptions
{
    public class AppException : Exception
    {
        public IEnumerable<string> Messages { get; }
        public StatusCode StatusCode { get; }

        public AppException(string Message, StatusCode StatusCode)
            : base(Message)
        {
            this.StatusCode = StatusCode;
        }
        public AppException(IEnumerable<string> Message, StatusCode StatusCode)
        {
            this.Messages = Message;
            this.StatusCode = StatusCode;
        }
    }
}
