﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.Common.CustomExtentions.Configure
{
    public static class ConfigurationExtention
    {
        public static string GetConnectionString(IConfiguration configuration, string ConnectionName)
        {
            return configuration.GetConnectionString(ConnectionName);
        }
    }
}
