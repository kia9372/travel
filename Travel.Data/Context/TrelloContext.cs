﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Travel.Common.CustomReflection;
using Travel.Entities.Base;
using Travel.Mapping.Base;

namespace Travel.Data.Context
{
    public class TravelContext : DbContext
    {
        public TravelContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.AddDbSet<IEntity>(typeof(IEntity).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IType).Assembly);
        }
    }
}
