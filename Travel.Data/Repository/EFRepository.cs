﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Data.Context;

namespace Travel.Data.Repository
{
    public class EFRepository<TEntity> : IEFRepository<TEntity> where TEntity : class
    {
        private readonly TravelContext _dbContext;
        public DbSet<TEntity> Entities { get; }
        public virtual IQueryable<TEntity> Table => Entities;
        public virtual IQueryable<TEntity> TableAsNoTracking => Entities.AsNoTracking();

        public EFRepository(TravelContext dbContext)
        {
            _dbContext = dbContext;
            Entities = _dbContext.Set<TEntity>();
        }

        #region AsyncMethod

        public async virtual Task<TEntity> GetByIdAsync(CancellationToken cancellationToken, params object[] ids)
        {
            return await Entities.FindAsync(ids, cancellationToken);
        }
        public virtual async Task<int> AddAsync(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true)
        {
            await Entities.AddAsync(entity, cancellationToken).ConfigureAwait(false);
            return await _dbContext.SaveChangesAsync();
        }
        public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true)
        {
            await Entities.AddRangeAsync(entities, cancellationToken).ConfigureAwait(false);
            if (SaveNow)
                await _dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        public virtual async Task<int> UpdateAsync(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.Update(entity);
            return await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task UpdateRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.UpdateRange(entities);
            if (SaveNow)
                await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task<int> DeleteAsync(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.Remove(entity);
            return await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.RemoveRange(entities);
            if (SaveNow)
            {
                var save = await _dbContext.SaveChangesAsync(cancellationToken);
            }

        }
        #endregion
        #region Sync Method
        public virtual TEntity GetById(CancellationToken canceletionToken, int ids)
        {
            return this.Entities.Find(ids, canceletionToken);
        }

        public virtual void Add(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.AddAsync(entity, cancellationToken).ConfigureAwait(false);
            if (SaveNow)
                _dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.AddRangeAsync(entities, cancellationToken).ConfigureAwait(false);
            if (SaveNow)
                _dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        public virtual void Update(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.Update(entity);
            if (SaveNow)
                _dbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual void UpdateRange(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.UpdateRange(entities);
            if (SaveNow)
                _dbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual void Delete(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.Remove(entity);
            if (SaveNow)
                _dbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual void DeleteRange(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true)
        {
            Entities.RemoveRange(entities);
            if (SaveNow)
                _dbContext.SaveChangesAsync(cancellationToken);
        }
        #endregion
        #region Attach and Deatach

        public virtual void Attach(TEntity entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Detached)
                Entities.Attach(entity);
        }

        public virtual void Deatach(TEntity entity)
        {
            var entry = _dbContext.Entry(entity);
            if (entry != null)
                entry.State = EntityState.Detached;
        }
        #endregion
    }
}
