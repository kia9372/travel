﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Travel.Data.Repository
{
    public interface IEFRepository<TEntity> where TEntity : class
    {
        DbSet<TEntity> Entities { get; }
        IQueryable<TEntity> Table { get; }
        IQueryable<TEntity> TableAsNoTracking { get; }
        void Add(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true);
        Task<int> AddAsync(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true);
        void AddRange(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true);
        Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true);
        void Attach(TEntity entity);
        void Deatach(TEntity entity);
        void Delete(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true);
        Task<int> DeleteAsync(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true);
        void DeleteRange(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true);
        Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true);
        Task<TEntity> GetByIdAsync(CancellationToken cancellationToken, params object[] ids);
        TEntity GetById(CancellationToken canceletionToken, int ids);
        void Update(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true);
        Task<int> UpdateAsync(TEntity entity, CancellationToken cancellationToken, bool SaveNow = true);
        void UpdateRange(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true);
        Task UpdateRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool SaveNow = true);
    }
}