﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Travel.Entities.Entites;
using Travel.Framework.Base;
using Travel.Framework.ResultApi;
using Travel.Services.RoleServices.Command;
using Travel.Services.RoleServices.Command.DeleteCommand;
using Travel.Services.RoleServices.Command.UpdateCommand;
using Travel.Services.RoleServices.Contract;
using Travel.Services.RoleServices.Query;
using Travel.Services.RoleServices.Query.GetOneQuery;
using Travel.Services.RoleServices.Service;
using Travel.ViewModel.RoleDtos;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Travel.Controllers.V1.RoleControllers
{
    public class RoleController : BaseController
    {
        private readonly IMediator mediator;

        public RoleController(IMediator mediator) : base(mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        public async Task<ReturnResult> AddRole(RoleDto model, CancellationToken token)
        {
            var add = await mediator.Send(new CreateRoleCommand { RoleName = model.Name }, token);

            if (add == 1)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<string> UpdateRole(EditRoleDto model, CancellationToken token)
        {
            return await mediator.Send(new UpdateRoleCommand
            {
                Id = model.Id,
                IsDelete = model.IsDelete,
                Name = model.Name
            },
            token);
        }

        [HttpGet]
        public async Task<IList<Role>> GetRoles()
        {
            return await mediator.Send(new GetAllRoleQuery());
        }

        [HttpGet]
        public async Task<Role> GetOneRole(int id)
        {
            return await mediator.Send(new GetOneQuery { Id = id });
        }

        [HttpDelete]
        public async Task<string> DeleteRole(int id, CancellationToken token)
        {
            return await mediator.Send(new DeleteCommand { Id = id }, token);
        }
    }
}
