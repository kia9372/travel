﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Travel.Entities.Entites;
using Travel.Framework.Base;
using Travel.Services.UserServices.Contract;

namespace Travel.Controllers.V1.UserControllers
{
    public class UserController : BaseController
    {
        private readonly IUserService userService;

        public UserController(IUserService userService, IMediator mediator) : base(mediator)
        {
            this.userService = userService;
        }

        [HttpGet]
        public async Task<List<User>> GetUsers()
        {
            return await userService.TableAsNoTracking.Where(x => !x.IsDelete).ToListAsync();
        }

        [HttpGet]
        public User GetUser(int id, CancellationToken token)
        {
            return userService.GetById(token, id);
        }
    }
}