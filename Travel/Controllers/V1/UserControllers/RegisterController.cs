﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Travel.Framework.Base;
using Travel.Framework.ResultApi;
using Travel.Services.UserServices.Contract;
using Travel.ViewModel.UserDto;

namespace Travel.Controllers.V1.UserControllers
{
    public class RegisterController : BaseController
    {
        private readonly IUserService userService;

        public RegisterController(IUserService userService, IMediator mediator) : base(mediator)
        {
            this.userService = userService;
        }

        [HttpPost]
        public async Task<string> RegisterUser(ResgiterDto dto, CancellationToken token)
        {
            return await userService.AddUser(dto, token);
        }
    }
}