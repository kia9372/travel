﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Travel.Framework.Base;
using Travel.Services.TokenServices.Contract;
using Travel.Services.UserServices.Contract;
using Travel.ViewModel.TokenDto;
using Travel.ViewModel.UserDto;

namespace Travel.Controllers.V1.UserControllers
{
    public class LoginController : BaseController
    {
        private readonly ITokenService token;
        private readonly IUserService user;

        public LoginController(ITokenService token, IUserService user , IMediator mediator):base(mediator)
        {
            this.token = token;
            this.user = user;
        }

        [HttpPost]
        public async Task<RefreshTokenDto> Login(LoginDto dto)
        {
            var find = user.FindByUsername(dto.Username);
            if (find != null)
            {
                var password = user.FindUSerPassword(dto.Password);
                if (password != null)
                {
                    return await token.GenerateNewToken(find);
                }
            }
            return null;
        }
    }
}