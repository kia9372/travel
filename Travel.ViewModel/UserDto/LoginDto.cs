﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.ViewModel.UserDto
{
    public class LoginDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
