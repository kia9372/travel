﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.ViewModel.UserDto
{
    public class ResgiterDto
    {
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Gender { get; set; }
        public DateTime Birthdate { get; set; }
    }
}
