﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.ViewModel.RoleDtos
{
    public class EditRoleDto
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public bool IsDelete { get; set; }
    }
}
