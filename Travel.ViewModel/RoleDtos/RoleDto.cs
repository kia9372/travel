﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.ViewModel.RoleDtos
{
    public class RoleDto
    {
        public string Name { get; set; }
    }
}
