﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.ViewModel.TokenDto
{
    public class TokenRequestDto
    {
        public int UserId { get; set; }
        public string ClientId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Value { get; set; }
    }
}
