﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.ViewModel.TokenDto
{
    public class RefreshTokenDto
    {
        public string Accesstoken { get; set; }
        public string RefreshToken { get; set; }
    }
}
