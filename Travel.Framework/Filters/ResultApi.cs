﻿using Travel.Common.CustomExtentions.Enums;
using Travel.Framework.ResultApi;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Travel.Framework.Filters
{
    public class ResultApi : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (context.Result is OkObjectResult okObjectResult)
            {
                var ReturnResult = new ReturnResult<object>(true, StatusCode.Success, okObjectResult.Value);
                context.Result = new JsonResult(ReturnResult) { StatusCode = okObjectResult.StatusCode };
            }
            else if (context.Result is OkResult okResult)
            {
                var ReturnResult = new ReturnResult(true, StatusCode.Success);
                context.Result = new JsonResult(ReturnResult) { StatusCode = okResult.StatusCode };
            }
            else if (context.Result is BadRequestResult badRequestResult)
            {
                var ReturnResult = new ReturnResult(false, StatusCode.BadRequest);
                context.Result = new JsonResult(ReturnResult) { StatusCode = badRequestResult.StatusCode };
            }
            else if (context.Result is BadRequestObjectResult badRequestObjectResult)
            {
                var message = badRequestObjectResult.Value.ToString();
                if (badRequestObjectResult.Value is SerializableError errors)
                {
                    var errorMessages = errors.SelectMany(p => (string[])p.Value).Distinct();
                    message = string.Join(" | ", errorMessages);
                }
                var ReturnResult = new ReturnResult(false, StatusCode.BadRequest, message);
                context.Result = new JsonResult(ReturnResult) { StatusCode = badRequestObjectResult.StatusCode };
            }
            else if (context.Result is ContentResult contentResult)
            {
                var ReturnResult = new ReturnResult(true, StatusCode.Success, contentResult.Content);
                context.Result = new JsonResult(ReturnResult) { StatusCode = contentResult.StatusCode };
            }
            else if (context.Result is NotFoundResult notFoundResult)
            {
                var ReturnResult = new ReturnResult(false, StatusCode.NotFound);
                context.Result = new JsonResult(ReturnResult) { StatusCode = notFoundResult.StatusCode };
            }
            else if (context.Result is NotFoundObjectResult notFoundObjectResult)
            {
                var ReturnResult = new ReturnResult<object>(false, StatusCode.NotFound, notFoundObjectResult.Value);
                context.Result = new JsonResult(ReturnResult) { StatusCode = notFoundObjectResult.StatusCode };
            }
            else if (context.Result is ObjectResult objectResult && objectResult.StatusCode == null
                && !(objectResult.Value is ReturnResult))
            {
                var ReturnResult = new ReturnResult<object>(true, StatusCode.Success, objectResult.Value);
                context.Result = new JsonResult(ReturnResult) { StatusCode = objectResult.StatusCode };
            }

            base.OnResultExecuting(context);
        }
    }
}
