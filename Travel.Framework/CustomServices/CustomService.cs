﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Travel.Data.Context;
using Travel.Services.RoleServices.Command;

namespace Travel.Framework.CustomServices
{
    public static class CustomService
    {

        public static void AddVersioning(this IServiceCollection services)
        {
            //services.AddApiVersioning(options =>
            //{
            //    options.AssumeDefaultVersionWhenUnspecified = true; //default => false;
            //    options.DefaultApiVersion = new ApiVersion(1, 0); //v1.0 == v1
            //    options.ReportApiVersions = true;
            //    ApiVersion.TryParse("1.0", out var version10);
            //    ApiVersion.TryParse("1", out var version1);
            //    var a = version10 == version1;

            //});
        }

        public static void AddContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<TravelContext>(options => options.UseSqlServer(configuration.GetConnectionString("SqlServer")));
        }

        public static void AddMediatR(this IServiceCollection services)
        {
            services.AddMediatR(typeof(CreateRoleCommand).GetTypeInfo().Assembly);
        }

        public static void AddRedis(this IServiceCollection services)
        {
            services.AddDistributedRedisCache(options =>
            {
                options.InstanceName = "Travel";
                options.Configuration = "local";
            });
        }
    }
}
