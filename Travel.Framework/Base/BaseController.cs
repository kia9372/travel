﻿using Travel.Framework.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Travel.Framework.Base
{
    [Route("[controller]/[action]")]
    [ApiController]
    [Travel.Framework.Filters.ResultApi]
    public class BaseController : ControllerBase
    {
        private readonly IMediator mediator;

        public BaseController(IMediator mediator)
        {
            this.mediator = mediator;
        }
    }
}
