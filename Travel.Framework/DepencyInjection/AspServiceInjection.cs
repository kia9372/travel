﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Context;
using Travel.Data.Repository;
using Travel.Services.RoleServices.Contract;
using Travel.Services.RoleServices.Service;
using Travel.Services.TokenServices.Contract;
using Travel.Services.TokenServices.Service;
using Travel.Services.UserRoleServices.Contract;
using Travel.Services.UserRoleServices.Service;
using Travel.Services.UserServices.Contract;
using Travel.Services.UserServices.Service;
using Travel.ViewModel.Settings;

namespace Travel.Framework.DepencyInjection
{
    public static class AspServiceInjection
    {
        public static void Injection(this IServiceCollection services)
        {
            services.AddScoped(typeof(IEFRepository<>), typeof(EFRepository<>));
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRoleService, UserRoleService>();
            services.AddScoped<ITokenService, TokenService>();
        }

      

    }
}
