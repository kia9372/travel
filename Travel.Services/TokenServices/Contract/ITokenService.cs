﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Travel.Data.Repository;
using Travel.Entities.Entites;
using Travel.ViewModel.TokenDto;

namespace Travel.Services.TokenServices.Contract
{
    public interface ITokenService : IEFRepository<UserToken>
    {
        Task<RefreshTokenDto> GenerateNewToken(User user);
    }
}
