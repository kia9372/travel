﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Data.Context;
using Travel.Data.Repository;
using Travel.Entities.Entites;
using Travel.Services.TokenServices.Contract;
using Travel.Services.UserServices.Contract;
using Travel.ViewModel.Settings;
using Travel.ViewModel.TokenDto;

namespace Travel.Services.TokenServices.Service
{
    public class TokenService : EFRepository<UserToken>, ITokenService
    {
        private readonly SiteSetting Snapshot;
        private readonly IUserService userService;

        public TokenService(TravelContext dbContext, IOptionsSnapshot<SiteSetting> snapshot, IUserService userService) : base(dbContext)
        {
            Snapshot = snapshot.Value;
            this.userService = userService;
        }

        public async Task<string> GenerationAsync(User user)
        {
            var securityByte = Encoding.UTF8.GetBytes(Snapshot.JwtSetting.SecretKey);
            var signInCreational = new SigningCredentials(new SymmetricSecurityKey(securityByte)
                , SecurityAlgorithms.HmacSha256Signature);

            var encriptKey = Encoding.UTF8.GetBytes(Snapshot.JwtSetting.Encryptkey);
            var encriptionCreational = new EncryptingCredentials(new SymmetricSecurityKey(encriptKey)
                , SecurityAlgorithms.Aes128KW, SecurityAlgorithms.Aes128CbcHmacSha256);

            ///تنظیم ویژگی های توکن
            var descriptor = new SecurityTokenDescriptor
            {
                ///صادر کننده توکن
                Issuer = Snapshot.JwtSetting.Issuer,
                Audience = Snapshot.JwtSetting.Audience,
                IssuedAt = DateTime.Now,
                NotBefore = DateTime.Now,
                Expires = DateTime.Now.AddMonths(1),
                EncryptingCredentials = encriptionCreational,
                SigningCredentials = signInCreational,
                Subject = new ClaimsIdentity(CustomClaims(user))
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            return tokenHandler.WriteToken(tokenHandler.CreateToken(descriptor));
        }

        public IEnumerable<Claim> CustomClaims(User user)
        {
            var securityStampClaimType = new ClaimsIdentityOptions().SecurityStampClaimType;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,user.UserName),
                new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                new Claim(securityStampClaimType,user.SecurityStamp.ToString())
            };
            return claims;
        }

        public async Task<RefreshTokenDto> GenerateNewToken(User user)
        {
            var newTokenGenerate = GenerateRefreshToken(user.Id);
            var userToken = await ValidateRefreshToken(newTokenGenerate.Value);
            if (userToken != null)
            {
                await DeleteAsync(userToken, CancellationToken.None);
            }
            await AddAsync(newTokenGenerate, CancellationToken.None);

            return new RefreshTokenDto
            {
                Accesstoken = await GenerationAsync(user),
                RefreshToken = await GenerationAsync(user)
            };
        }

        public UserToken GenerateRefreshToken(int userId)
        {
            return new UserToken
            {
                IsDelete = false,
                UserId = userId,
                ClientId = Snapshot.JwtSetting.ClientId,
                ExpireDate = DateTime.Now.AddDays(7),
                Value = Guid.NewGuid().ToString("N")
            };
        }

        public async Task<UserToken> ValidateRefreshToken(string refreshToken)
        {
            var findRefreshToken = await TableAsNoTracking.Where(x => x.Value == refreshToken).FirstOrDefaultAsync();
            if (findRefreshToken != null)
            {
                return findRefreshToken;
            }
            return null;
        }

    }
}
