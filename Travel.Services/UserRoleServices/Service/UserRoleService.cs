﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Data.Context;
using Travel.Data.Repository;
using Travel.Entities.Entites;
using Travel.Messages.RoleResx;
using Travel.Messages.UserResx;
using Travel.Messages.UserRoleResx;
using Travel.Services.RoleServices.Contract;
using Travel.Services.UserRoleServices.Contract;
using Travel.Services.UserServices.Contract;

namespace Travel.Services.UserRoleServices.Service
{
    public class UserRoleService : EFRepository<UserRole>, IUserRoleService
    {
        private readonly IRoleService roleService;

        public UserRoleService(TravelContext dbContext, IRoleService roleService) : base(dbContext)
        {
            this.roleService = roleService;
        }

        public async Task<int> AddUserRole(int UserId, int RoleId, CancellationToken token)
        {
            var role = roleService.FindRoleByID(RoleId);
            if (role != null)
            {
                #region UserRole
                UserRole userRole = new UserRole();
                userRole.IsDelete = false;
                userRole.RoleId = RoleId;
                userRole.UserId = UserId;
                #endregion
                return await AddAsync(userRole, token);
            }
            else
            {
                return 0;
            }
        }
    }
}
