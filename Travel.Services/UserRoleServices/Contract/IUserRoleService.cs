﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Data.Repository;
using Travel.Entities.Entites;

namespace Travel.Services.UserRoleServices.Contract
{
    public interface IUserRoleService : IEFRepository<UserRole>
    {
        Task<int> AddUserRole(int userId, int RoleId, CancellationToken token);
    }
}
