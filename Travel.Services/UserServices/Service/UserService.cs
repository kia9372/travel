﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Travel.Data.Context;
using Travel.Data.Repository;
using Travel.Entities.Entites;
using Travel.Messages.UserResx;
using Travel.Services.UserRoleServices.Contract;
using Travel.Services.UserServices.Contract;
using Travel.ViewModel.UserDto;

namespace Travel.Services.UserServices.Service
{
    public class UserService : EFRepository<User>, IUserService
    {
        private readonly TravelContext dbContext;
        private readonly IUserRoleService userRoleService;

        public UserService(TravelContext dbContext, IUserRoleService userRoleService) : base(dbContext)
        {
            this.dbContext = dbContext;
            this.userRoleService = userRoleService;
        }

        public async Task<string> AddUser(ResgiterDto dto, CancellationToken token)
        {
            #region InitialUser
            User user = new User();
            user.Email = dto.Email;
            user.FirstName = dto.Firstname;
            user.LastName = dto.Lastname;
            user.Password = dto.Password;
            user.UserName = dto.Username;
            user.Birthdate = dto.Birthdate;
            #endregion

            using (var transaction = dbContext.Database.BeginTransaction())
            {
                var add = await AddAsync(user, token);
                if (add == 1)
                {
                    var userFind = FindByUsername(user.UserName);
                    if (userFind != null)
                    {
                        var addUserRole = await userRoleService.AddUserRole(userFind.Id, 1, token);
                        if (addUserRole == 1)
                        {
                            transaction.Commit();
                            return string.Format(UserMessages.Success_Register_User);
                        }
                        else
                        {
                            return string.Format(UserMessages.Fail_Register_User);
                        }
                    }
                    else
                    {
                        return string.Format(UserMessages.NotFound_User);
                    }
                }
                else
                {
                    return string.Format(UserMessages.Fail_Register_User);
                }
            }
        }

        public User FindByUsername(string Username)
        {
            return TableAsNoTracking.FirstOrDefault(x => x.UserName == Username);
        }

        public User FindUSerPassword(string password)
        {
            return TableAsNoTracking.FirstOrDefault(x => x.Password == password);
        }

        public async Task SecurityStampUpdate(int userId, CancellationToken token)
        {
            var user = await GetByIdAsync(token, userId);
            if (user != null)
            {
                user.SecurityStamp = Guid.NewGuid();
                await UpdateAsync(user, token);
            }
        }

    }
}
