﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Data.Repository;
using Travel.Entities.Entites;
using Travel.ViewModel.UserDto;

namespace Travel.Services.UserServices.Contract
{
    public interface IUserService : IEFRepository<User>
    {
        Task<string> AddUser(ResgiterDto dto, CancellationToken token);
        User FindByUsername(string Username);
        Task SecurityStampUpdate(int userId, CancellationToken token);
        User FindUSerPassword(string password);
    }
}
