﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Travel.Common.DepencyLifeTime;
using Travel.Data.Context;
using Travel.Data.Repository;
using Travel.Entities.Entites;
using Travel.Messages.Public;
using Travel.Messages.RoleResx;
using Travel.Services.RoleServices.Contract;
using Travel.ViewModel.RoleDtos;

namespace Travel.Services.RoleServices.Service
{
    public class RoleService : EFRepository<Role>, IRoleService, ISinglton
    {
        public RoleService(TravelContext dbContext) : base(dbContext)
        {
        }
        public async Task<string> AddRole(RoleDto dto, CancellationToken token)
        {
            #region InitialRole
            Role role = new Role();
            role.RoleName = dto.Name;
            #endregion

            var add = await AddAsync(role, token);
            if (add == 1)
            {
                return string.Format(RoleMessages.Success_Add_Role);
            }
            else
            {
                return string.Format(RoleMessages.Fail_Add_Role);
            }
        }

        public async Task<string> UpdateRoleAsync(Role model, CancellationToken token)
        {
            var role = await GetByIdAsync(token, model.Id);
            if (role != null)
            {
                role.RoleName = model.RoleName;
                role.SecurityStamp = Guid.NewGuid();
                var add = await UpdateAsync(role, token);
                if (add == 1)
                {
                    return string.Format(RoleMessages.Update_Role_Success);
                }
                else
                {
                    return string.Format(RoleMessages.Update_Role_Fail);
                }
            }
            return string.Format(PublicMessage.BadRequest_Message);
        }

        public async Task<string> DeleteRoleAsync(Role model, CancellationToken token)
        {
            var role = await GetByIdAsync(token, model.Id);
            if (role != null)
            {
                role.IsDelete = true;
                var add = await UpdateAsync(role, token);
                if (add == 1)
                {
                    return string.Format(RoleMessages.Delete_Role_Success);
                }
                else
                {
                    return string.Format(RoleMessages.Delete_Role_Fail);
                }
            }
            return string.Format(PublicMessage.BadRequest_Message);
        }

        public Role FindRoleByID(int id)
        {
            return TableAsNoTracking.FirstOrDefault(x => x.Id == id);
        }
    }
}
