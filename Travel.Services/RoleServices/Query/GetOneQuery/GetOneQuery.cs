﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Travel.Entities.Entites;
using Travel.ViewModel.RoleDtos;

namespace Travel.Services.RoleServices.Query.GetOneQuery
{
    public class GetOneQuery : IRequest<Role>
    {
        public int Id { get; set; }
    }
}
