﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Entities.Entites;
using Travel.Services.RoleServices.Contract;

namespace Travel.Services.RoleServices.Query.GetOneQuery
{
    public class GetOneQueryCommand : IRequestHandler<GetOneQuery, Role>
    {
        private readonly IRoleService roleService;

        public GetOneQueryCommand(IRoleService roleService)
        {
            this.roleService = roleService;
        }
        public Task<Role> Handle(GetOneQuery request, CancellationToken cancellationToken)
        {
            return roleService.GetByIdAsync(cancellationToken, request.Id);
        }
    }
}
