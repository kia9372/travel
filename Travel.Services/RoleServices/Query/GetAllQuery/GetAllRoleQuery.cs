﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Travel.Entities.Entites;

namespace Travel.Services.RoleServices.Query
{
    public class GetAllRoleQuery : IRequest<IList<Role>>
    {
    }
}
