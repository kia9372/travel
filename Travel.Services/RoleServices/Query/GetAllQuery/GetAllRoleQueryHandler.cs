﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Entities.Entites;
using Travel.Services.RoleServices.Contract;

namespace Travel.Services.RoleServices.Query
{
    public class GetAllRoleQueryHandler : IRequestHandler<GetAllRoleQuery, IList<Role>>
    {
        private readonly IRoleService roleService;

        public GetAllRoleQueryHandler(IRoleService roleService)
        {
            this.roleService = roleService;
        }
        public async Task<IList<Role>> Handle(GetAllRoleQuery request, CancellationToken cancellationToken)
        {
            return await roleService.TableAsNoTracking.Where(x => x.IsDelete != true).ToListAsync();
        }
    }
}
