﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.Services.RoleServices.Command.DeleteCommand
{
    public class DeleteCommand : IRequest<string>
    {
        public int Id { get; set; }
    }
}
