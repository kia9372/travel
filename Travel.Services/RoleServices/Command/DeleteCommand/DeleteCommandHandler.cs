﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Entities.Entites;
using Travel.Services.RoleServices.Contract;

namespace Travel.Services.RoleServices.Command.DeleteCommand
{
    public class DeleteCommandHandler : IRequestHandler<DeleteCommand, string>
    {
        private readonly IRoleService roleService;

        public DeleteCommandHandler(IRoleService roleService)
        {
            this.roleService = roleService;
        }
        public Task<string> Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            #region InitialRole
            Role role = new Role();
            role.Id = request.Id;
            #endregion
            return roleService.DeleteRoleAsync(role, cancellationToken);
        }
    }
}
