﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.Services.RoleServices.Command.UpdateCommand
{
    public class UpdateRoleCommand : IRequest<string>
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public bool IsDelete { get; set; }
    }
}
