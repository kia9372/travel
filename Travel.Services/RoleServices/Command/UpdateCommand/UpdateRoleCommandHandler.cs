﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Entities.Entites;
using Travel.Services.RoleServices.Contract;

namespace Travel.Services.RoleServices.Command.UpdateCommand
{
    public class UpdateRoleCommandHandler : IRequestHandler<UpdateRoleCommand, string>
    {
        private readonly IRoleService roleService;

        public UpdateRoleCommandHandler(IRoleService roleService)
        {
            this.roleService = roleService;
        }
        public async Task<string> Handle(UpdateRoleCommand request, CancellationToken cancellationToken)
        {
            #region InitialRole
            Role role = new Role();
            role.Id = request.Id;
            role.IsDelete = request.IsDelete;
            role.RoleName = request.Name;
            #endregion
            return await roleService.UpdateRoleAsync(role, cancellationToken);
        }
    }
}
