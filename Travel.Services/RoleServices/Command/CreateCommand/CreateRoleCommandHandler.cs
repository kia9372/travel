﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Entities.Entites;
using Travel.Services.RoleServices.Contract;

namespace Travel.Services.RoleServices.Command
{
    public class CreateRoleCommandHandler : IRequestHandler<CreateRoleCommand, int>
    {
        private readonly IRoleService roleService;

        public CreateRoleCommandHandler(IRoleService roleService)
        {
            this.roleService = roleService;
        }
        public async Task<int> Handle(CreateRoleCommand request, CancellationToken cancellationToken)
        {
            #region InitialRole
            Role role = new Role();
            role.RoleName = request.RoleName;
            #endregion
            return await roleService.AddAsync(role, cancellationToken);
        }
    }
}
