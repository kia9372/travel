﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Travel.Entities.Entites;

namespace Travel.Services.RoleServices.Command
{
    public class CreateRoleCommand : IRequest<int>
    {
        public string RoleName { get; set; }
    }
}
