﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Travel.Common.DepencyLifeTime;
using Travel.Data.Repository;
using Travel.Entities.Entites;
using Travel.ViewModel.RoleDtos;

namespace Travel.Services.RoleServices.Contract
{
    public interface IRoleService: IEFRepository<Role>
    {
        Task<string> AddRole(RoleDto dto, CancellationToken token);
        Task<string> UpdateRoleAsync(Role model, CancellationToken token);
        Role FindRoleByID(int id);
        Task<string> DeleteRoleAsync(Role model, CancellationToken token);
    }
}
