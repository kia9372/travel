﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Travel.Entities.Base;

namespace Travel.Entities.Entites
{
    public class User : BaseEntite
    {
        public User()
        {
            IsDelete = false;
            SecurityStamp = Guid.NewGuid();
            IsActive = false;
        }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [StringLength(50)]
        public string UserName { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        public DateTime Birthdate { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }
        public Guid SecurityStamp { get; set; }
        public ICollection<UserRole> UserRole { get; set; }
        public ICollection<UserToken> UserTokens { get; set; }

    }
}
