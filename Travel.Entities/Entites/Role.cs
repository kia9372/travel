﻿using System;
using System.Collections.Generic;
using Travel.Entities.Base;

namespace Travel.Entities.Entites
{
    public class Role : BaseEntite
    {
        public Role()
        {
            IsDelete = false;
            SecurityStamp = Guid.NewGuid();
        }
        public string RoleName { get; set; }
        public Guid SecurityStamp { get; set; }
        public ICollection<UserRole> UserRole { get; set; }
    }
}
