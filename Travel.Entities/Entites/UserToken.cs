﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Entities.Base;

namespace Travel.Entities.Entites
{
    public class UserToken : BaseEntite
    {
        public int UserId { get; set; }
        public string ClientId { get; set; }
        public string Value { get; set; }
        public DateTime ExpireDate { get; set; }
        public User User { get; set; }
    }
}
