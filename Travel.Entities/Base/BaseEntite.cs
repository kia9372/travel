﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.Entities.Base
{
    public interface IEntity
    {
    }
    public abstract class BaseEntite<TData> : IEntity
    {
        public TData Id { get; set; }
        public bool IsDelete { get; set; }
    }
    public abstract class BaseEntite : BaseEntite<int>
    {

    }
}
